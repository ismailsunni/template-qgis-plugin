# Development

## Environment setup

Typically on Ubuntu:

```bash
# create virtual environment
python3.8 -m venv .venv
source .venv/bin/activate

# bump dependencies inside venv
python -m pip install -U pip setuptools wheel
python -m pip install -U -r requirements.txt
python -m pip install -U -r requirements/development.txt

# install project as editable
python -m pip install -e .
```
