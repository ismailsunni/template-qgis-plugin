# QGIS Plugin template

[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![flake8](https://img.shields.io/badge/linter-flake8-green)](https://flake8.pycqa.org/)
[![Imports: isort](https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336)](https://pycqa.github.io/isort/)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)

Repository template for developing QGIS plugins using modern and accepted "good" practices about tooling, code structuration, documentation and tests.

It's based on the amazing project templating engine: [Cookiecutter](https://cookiecutter.readthedocs.io/).

![Cookiecutter logo](https://raw.githubusercontent.com/cookiecutter/cookiecutter/3ac078356adf5a1a72042dfe72ebfa4a9cd5ef38/logo/cookiecutter_medium.png "Cookiecutter logo")

## Requirements

- Python 3.7+
- Recomended: Git

## How to use

In a nutshell:

```bash
# install cookiecutter
python -m pip install "cookiecutter>=1.7.0"
# run cookiecutter with this repository
cookiecutter https://gitlab.com/Oslandia/qgis/template-qgis-plugin
# if you don't have installed Git, use the ZIP URL
cookiecutter https://gitlab.com/Oslandia/qgis/template-qgis-plugin/-/archive/master/template-qgis-plugin-master.zip
```

You'll be prompted for some values. Provide them, then a QGIS plugin project folder  (named `plugin_[plugin_name_slugified]`) will be created for you.  

It's also possible to override the prompt:

```bash
cookiecutter --no-input -f https://gitlab.com/Oslandia/qgis/template-qgis-plugin plugin_name=my_latest_plugin
```

In this case, the output folder is: `plugin_my_latest_plugin`.

**Interested**? For further details, [read the documentation](https://oslandia.gitlab.io/qgis/template-qgis-plugin) :books:.

## Contribute

Read the [contribution guidelines](CONTRIBUTING.md) and the documentation.
