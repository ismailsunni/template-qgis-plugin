#! python3  # noqa: E265

"""
    Executed before templating operations in cookiecutter.

    See: https://cookiecutter.readthedocs.io/en/stable/advanced/hooks.html

    Inspired by https://github.com/pydanny/cookiecutter-django
"""

# ############################################################################
# ########## Libraries #############
# ##################################

# standard library
import logging
import shutil
import subprocess
from asyncio.log import logger
from pathlib import Path
from sys import platform as opersys
from sys import version_info

# ############################################################################
# ########## Globals ###############
# ##################################

# log customization
if "{{ cookiecutter.debug }}".startswith("y"):
    log_level = logging.DEBUG
else:
    log_level = logging.INFO

log_format = "[%(asctime)s] [%(levelname)s] - %(message)s"
if version_info.minor < 8:
    logging.basicConfig(level=log_level, format=log_format)
else:
    # only python >= 3.8
    logging.basicConfig(level=log_level, format=log_format, force=True)

if log_level == 10:
    logging.debug("Debug mode enabled")


# ############################################################################
# ########## Functions #############
# ##################################


def remove_gitlab_ci():
    """Clean up GitLab CI files"""
    gitlab_ci_files = [
        Path(".gitlab-ci.yml"),
    ]
    for f in gitlab_ci_files:
        if version_info.minor < 8:
            f.unlink()
        else:
            # only python >= 3.8
            f.unlink(missing_ok=True)  # only since Python >= 3.8
        logging.debug(f"{f} deleted")

    shutil.rmtree(".gitlab")
    logging.info("GitLab CI removed.")


def remove_dotgithub_folder():
    """Remove `.github` folder."""
    shutil.rmtree(".github")
    logging.info("GitHub configuration removed.")


def remove_linters_folder():
    """Remove `linters` folder if PyLint is not selected."""
    shutil.rmtree("linters")
    logging.info("PyLint configuration removed.")


def remove_processing():
    """Remove processing plugin"""
    shutil.rmtree("{{cookiecutter.plugin_name_slug}}/processing")
    logging.info("Processing provider module removed.")


# ############################################################################
# ########## Main ##################
# ##################################


def main():
    """Main script."""
    # CI/CD clean up
    if "{{ cookiecutter.ci_cd_tool }}".lower() != "gitlab":
        remove_gitlab_ci()

    if "{{ cookiecutter.ci_cd_tool }}".lower() != "github":
        remove_dotgithub_folder()

    # plugin type
    if "{{ cookiecutter.plugin_processing }}".startswith("n"):
        remove_processing()

    if "{{ cookiecutter.linter_py }}".lower() == "flake8":
        remove_linters_folder()

    # post actions
    try:
        if "{{ cookiecutter.post_git_init }}".startswith("y"):
            logging.info("Initializing git repository...")

            # git init
            subprocess.run(
                [shutil.which("git") or "git", "init"],
                stdout=subprocess.DEVNULL,
                stderr=subprocess.DEVNULL,
            )
            logger.info("Git repository initialized.")

            # add remote origin
            subprocess.run(
                [
                    shutil.which("git") or "git",
                    "remote",
                    "add",
                    "origin",
                    "{{cookiecutter.repository_url_base}}",
                ],
                capture_output=True,
                stdout=subprocess.DEVNULL,
                stderr=subprocess.DEVNULL,
            )
            logger.info("Git remote URL set: {{cookiecutter.repository_url_base}}")
    except Exception as err:
        logging.error(f"git init failed. Trace: {err}")

    try:
        if "{{ cookiecutter.post_install_venv }}".startswith("y"):
            logging.info("Creating virtual environment...")
            if opersys == "win32":
                subprocess.run(
                    [shutil.which("python") or "py", "-3.7", "-m", "venv", ".venv"],
                    stdout=subprocess.DEVNULL,
                    stderr=subprocess.DEVNULL,
                )

            else:
                subprocess.run(
                    [
                        shutil.which("python") or "python3",
                        "-m",
                        "venv",
                        ".venv",
                        "--system-site-packages",
                    ],
                    stdout=subprocess.DEVNULL,
                    stderr=subprocess.DEVNULL,
                )
    except Exception as err:
        logging.error(f"python -m venv failed. Trace: {err}")

    # let's say good bye
    logging.info(
        "Plugin {{cookiecutter.plugin_name_slug}} initialized: {}. "
        "Keep up the good work!".format(Path(".").resolve())
    )


# #############################################################################
# ##### Main #######################
# ##################################
if __name__ == "__main__":
    main()
