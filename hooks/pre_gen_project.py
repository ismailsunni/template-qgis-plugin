#! python3  # noqa: E265

"""
    Executed before templating operations in cookiecutter.

    See: https://cookiecutter.readthedocs.io/en/stable/advanced/hooks.html
"""

# ############################################################################
# ########## Libraries #############
# ##################################

# standard library
import sys
from pathlib import Path

# ############################################################################
# ########## Main ##################
# ##################################

# check plugin name
plugin_name_slug = "{{ cookiecutter.plugin_name_slug }}"
if hasattr(plugin_name_slug, "isidentifier") and not plugin_name_slug.isidentifier():
    sys.exit(
        "'{}' project slug is not a valid Python identifier.".format(plugin_name_slug)
    )

if not plugin_name_slug == plugin_name_slug.lower():
    sys.exit("'{}' project slug should be all lowercase".format(plugin_name_slug))

if "\\" in "{{ cookiecutter.author_name }}":
    sys.exit("Don't include backslashes in author name.")

# check if icon exists or not
plugin_icon = Path("{{ cookiecutter.plugin_icon }}")
if not plugin_icon.is_file():
    # sys.exit("Icon doesn't exist")
    "{{ cookiecutter.update({ 'plugin_icon': 'default_icon.png' }) }}"

# check repository url
repository_url = "{{ cookiecutter.repository_url_base }}"
if not repository_url.endswith("/"):
    repository_url = repository_url + "/"
if "github.com" in repository_url:
    url_issues = f"{repository_url}issues"
    "{{ cookiecutter.update({ 'repository_url_issues': '{}' }) }}".format(url_issues)
    "{{ cookiecutter.update({ 'repository_url_pages': '{}' }) }}"
elif "gitlab.com" in repository_url:
    url_issues = f"{repository_url}-/issues"
    "{{ cookiecutter.update({ 'repository_url_pages': none }) }}"
    "{{ cookiecutter.update({ 'repository_url_issues': none }) }}"
else:
    "{{ cookiecutter.update({ 'repository_url_pages': none }) }}"
    "{{ cookiecutter.update({ 'repository_url_issues': none }) }}"
