# {{ cookiecutter.plugin_name }} - QGIS Plugin

[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![Imports: isort](https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336)](https://pycqa.github.io/isort/)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)

{% if cookiecutter.linter_py == 'PyLint' or cookiecutter.linter_py == 'both' %}[![pylint]({{ cookiecutter.repository_url_base }}lint/pylint.svg)]({{ cookiecutter.repository_url_pages }}lint/){% endif %}
{% if cookiecutter.linter_py == 'Flake8' or cookiecutter.linter_py == 'both' %}[![flake8](https://img.shields.io/badge/linter-flake8-green)](https://flake8.pycqa.org/){% endif %}

## Generated options

### Plugin

"plugin_name": {{ cookiecutter.plugin_name }}
"plugin_name_slug": {{ cookiecutter.plugin_name_slug }}
"plugin_name_class": {{ cookiecutter.plugin_name_class }}

"plugin_category": {{ cookiecutter.plugin_category }}
"plugin_description_short": {{ cookiecutter.plugin_description_short }}
"plugin_description_long": {{ cookiecutter.plugin_description_long }}
"plugin_description_short": {{ cookiecutter.plugin_description_short }}
"plugin_icon": {{ cookiecutter.plugin_icon }}

"author_name": {{ cookiecutter.author_name }}
"author_email": {{ cookiecutter.author_email }}

"qgis_version_min": {{ cookiecutter.qgis_version_min }}
"qgis_version_max": {{ cookiecutter.qgis_version_max }}

### Tooling

This project is configured with the following tools:

- [Black](https://black.readthedocs.io/en/stable/) to format the code without any existential question
- [iSort](https://pycqa.github.io/isort/) to sort the Python imports

Code rules are enforced with [pre-commit](https://pre-commit.com/) hooks.{% if cookiecutter.linter_py != 'None' %}  
Static code analisis is based on: {{ cookiecutter.linter_py }}{%- endif %}

See also: [contribution guidelines](CONTRIBUTING.md).

{% if cookiecutter.ci_cd_tool != 'None' %}## CI/CD

Plugin is linted, tested, packaged and published with {{ cookiecutter.ci_cd_tool }}.

If you mean to deploy it to the [official QGIS plugins repository](https://plugins.qgis.org/), remember to set your OSGeo credentials (`OSGEO_USER_NAME` and `OSGEO_USER_PASSWORD`) as environment variables in your CI/CD tool. 
{% endif %}

### Documentation

The documentation is generated using Sphinx and is automatically generated through the CI and published on Pages.

- homepage: <{{ cookiecutter.repository_url_pages }}>
- repository: <{{ cookiecutter.repository_url_base }}>
- tracker: <{{ cookiecutter.repository_url_issues }}>

----

## License

Distributed under the terms of the [`{{ cookiecutter.open_source_license }}` license](LICENSE).
