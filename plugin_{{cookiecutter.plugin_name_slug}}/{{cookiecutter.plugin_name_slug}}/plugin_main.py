#! python3  # noqa: E265

"""
    Main plugin module.
"""

# PyQGIS
from qgis.core import QgsApplication
from qgis.gui import QgisInterface
from qgis.PyQt.QtCore import QCoreApplication
from qgis.PyQt.QtGui import QIcon
from qgis.PyQt.QtWidgets import QAction
from qgis.utils import showPluginHelp

# project
from {{cookiecutter.plugin_name_slug}}.__about__ import __title__
from {{cookiecutter.plugin_name_slug}}.gui.dlg_settings import PlgOptionsFactory
{% if cookiecutter.plugin_processing == "yes" %}
from {{cookiecutter.plugin_name_slug}}.processing import {{ cookiecutter.plugin_name_class }}Provider
{% endif %}
from {{cookiecutter.plugin_name_slug}}.toolbelt import PlgLogger, PlgTranslator

# ############################################################################
# ########## Classes ###############
# ##################################


class {{ cookiecutter.plugin_name_class }}Plugin:
    def __init__(self, iface: QgisInterface):
        """Constructor.

        :param iface: An interface instance that will be passed to this class which \
        provides the hook by which you can manipulate the QGIS application at run time.
        :type iface: QgsInterface
        """
        self.iface = iface
        self.log = PlgLogger().log
        {% if cookiecutter.plugin_processing == "yes" %}self.provider = None{% endif %}

        # translation
        plg_translation_mngr = PlgTranslator()
        translator = plg_translation_mngr.get_translator()
        if translator:
            QCoreApplication.installTranslator(translator)
        self.tr = plg_translation_mngr.tr

    def initGui(self):
        """Set up plugin UI elements."""

        # settings page within the QGIS preferences menu
        self.options_factory = PlgOptionsFactory()
        self.iface.registerOptionsWidgetFactory(self.options_factory)

        # -- Actions
        self.action_help = QAction(
            QIcon(":/images/themes/default/mActionHelpContents.svg"),
            self.tr("Help", context="{{ cookiecutter.plugin_name_class }}Plugin"),
            self.iface.mainWindow(),
        )
        self.action_help.triggered.connect(
            lambda: showPluginHelp(filename="resources/help/index")
        )

        self.action_settings = QAction(
            QgsApplication.getThemeIcon("console/iconSettingsConsole.svg"),
            self.tr("Settings"),
            self.iface.mainWindow(),
        )
        self.action_settings.triggered.connect(
            lambda: self.iface.showOptionsDialog(
                currentPage="mOptionsPage{}".format(__title__)
            )
        )

        # -- Menu
        self.iface.addPluginToMenu(__title__, self.action_settings)
        self.iface.addPluginToMenu(__title__, self.action_help)

        {% if cookiecutter.plugin_processing == "yes" %}# -- Processing
        self.initProcessing(){% endif %}

    {% if cookiecutter.plugin_processing == "yes" %}
    def initProcessing(self):
        self.provider = {{ cookiecutter.plugin_name_class }}Provider()
        QgsApplication.processingRegistry().addProvider(self.provider){% endif %}

    def unload(self):
        """Cleans up when plugin is disabled/uninstalled."""
        # -- Clean up menu
        self.iface.removePluginMenu(__title__, self.action_help)
        self.iface.removePluginMenu(__title__, self.action_settings)

        # -- Clean up preferences panel in QGIS settings
        self.iface.unregisterOptionsWidgetFactory(self.options_factory)

        {% if cookiecutter.plugin_processing == "yes" %}# -- Unregister processing
        QgsApplication.processingRegistry().removeProvider(self.provider){% endif %}

        # remove actions
        del self.action_settings
        del self.action_help

    def run(self):
        """Main process.

        :raises Exception: if there is no item in the feed
        """
        try:
            self.log(
                message=self.tr(
                    text="Everything ran OK.",
                    context="{{ cookiecutter.plugin_name_class }}Plugin",
                ),
                log_level=3,
                push=False,
            )
        except Exception as err:
            self.log(
                message=self.tr(
                    text="Houston, we've got a problem: {}".format(err),
                    context="{{ cookiecutter.plugin_name_class }}Plugin",
                ),
                log_level=2,
                push=True,
            )
