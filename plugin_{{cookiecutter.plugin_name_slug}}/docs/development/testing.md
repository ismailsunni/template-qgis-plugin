# Testing the plugin

Tests are written in 2 separate folders:

- `tests/unit`: testing code which is independent of QGIS API
- `tests/qgis`: testing code which depends on QGIS API

## Requirements

- {{ cookiecutter.qgis_version_min }} < QGIS < {{ cookiecutter.qgis_version_max }}

```bash
python -m pip install -U -r requirements/testing.txt
```

## Run unit tests

```bash
# run all tests with PyTest and Coverage report
python -m pytest

# run only unit tests with pytest launcher
python -m pytest tests/unit

# run only QGIS tests with pytest launcher
python -m pytest tests/qgis

# run a specific test module using standard unittest
python -m unittest tests.unit.test_plg_metadata

# run a specific test function using standard unittest
python -m unittest tests.unit.test_plg_metadata.TestPluginMetadata.test_version_semver
```
